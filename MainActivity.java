package com.example.evotec.practica4_pm_bjcp;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragmentUno.OnFragmentInteractionListener, FragmentDos.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnFDos = (Button)findViewById(R.id.btnFDos);
        btnFUno = (Button)findViewById(R.id.btnFUno);

        btnFUno.setOnClickListener(this);
        btnFDos.setOnClickListener(this);

    }
    Button btnFDos, btnFUno;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFUno:
                FragmentUno fragUno = new FragmentUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragUno);
                transactionUno.commit();
                break;

            case R.id.btnFDos:
                FragmentDos fragDos = new FragmentDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_log);
               Button botonAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText)dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText)dialogoLogin.findViewById(R.id.txtPass);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
               dialogoLogin.show();

                break;
            case R.id.opcionRegistrar:
               // Dialog dialogoRegistrar = new Dialog(MainActivity.this);
               // dialogoRegistrar.setContentView(R.layout.dlg_regis);
               // dialogoRegistrar.show();

                break;
        }
        return true;
    }
}
